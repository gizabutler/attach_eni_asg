import pprint
import boto3
from botocore.exceptions import ClientError

# BOTO_REGION = os.environ["BOTO_REGION"]

# if BOTO_REGION == "":
#     BOTO_REGION = os.environ["AWS_DEFAULT_REGION"]

ec2 = boto3.client('ec2', region_name='us-east-2')
eni_tag_key = "Name"
eni_tag_value = "foxpass-vpn"
subnet_id = ""


def get_free_interfaces(ec2,):

    return ec2.describe_network_interfaces(
        Filters=[
            {
               	"Name": "tag:" + eni_tag_key,
                "Values": [eni_tag_value]
            },
            {
                "Name": 'subnet-id',
                "Values": [subnet_id]
            },
            {
                "Name": "status",
                "Values": ["available"]
            }

        ]
    )['NetworkInterfaces']


print(get_free_interfaces(ec2)[0]['NetworkInterfaceId'])
