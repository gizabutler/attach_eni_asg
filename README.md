# attach_eni_asg

This is a lambda function that will automatically add available ENIs to instances that are created as part of an autoscaling group

This is based off of an amazon knowledge base article [How do I automatically attach a second elastic network interface (ENI) to an instance launched through Auto Scaling?](https://aws.amazon.com/premiumsupport/knowledge-center/attach-second-eni-auto-scaling/)

This lambda function has been modified to search for existing available network interfaces that match the subnet of the new autoscaled instance as opposed to creating a new one. 